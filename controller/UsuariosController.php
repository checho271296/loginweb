<?php


class UsuariosController extends ControladorBase{
    public $conectar;
    public $adapter;
    public $errors;
    public function __construct() {
        parent::__construct();
        $this->conectar=new Conectar();
        $this->adapter=$this->conectar->conexion();
    }
     
    public function index(){
         
        //Creamos el objeto usuario
        $usuario=new Usuario($this->adapter);
         
        //Conseguimos todos los usuarios
        $allusers=$usuario->getAll();
        

        
        //Cargamos la vista index y le pasamos valores
        $this->view("index",array(
            "loginError" => "",
            "passError" => "",
            "passChange" => "",
            "createUser" => "",
            "userRepet" => "" 
        ),1);
    }

    public function viewErrors(){
         
        //Creamos el objeto usuario
        $usuario=new Usuario($this->adapter);
         
        
        //Cargamos la vista index y le pasamos valores
        $this->view("index",array(
            "loginError" => "*Usuario o contraseña incorrecta!",
            "passError" => "",
            "passChange" => "",
            "createUser" => "",
            "userRepet" => "" 
        ),1);
    }
    public function viewAccept(){
         
        //Creamos el objeto usuario
        $usuario=new Usuario($this->adapter);
         
        
        //Cargamos la vista index y le pasamos valores
        $this->view("index",array(
            "loginError" => "",
            "passError" => "",
            "passChange" => "*Contraseña cambiada correctamente!",
            "createUser" => "",
            "userRepet" => "" 
        ),1);
    }

    public function viewAcceptUser(){
         
        //Creamos el objeto usuario
        $usuario=new Usuario($this->adapter);
         
        
        //Cargamos la vista index y le pasamos valores
        $this->view("index",array(
            "loginError" => "",
            "passError" => "",
            "passChange" => "",
            "createUser" => "Usuario creado correctamente",
            "userRepet" => ""            
        ),1);
    }

    public function viewPassError(){
         
        //Creamos el objeto usuario
        $usuario=new Usuario($this->adapter);
         
        
        //Cargamos la vista index y le pasamos valores
        $this->view("index",array(
            "loginError" => "",
            "passError" => "*Contraseñas nuevas no coinciden!",
            "passChange" => "",
            "createUser" => "",
            "userRepet" => ""
        ),1);
    }
    public function userRepetido(){
         
        //Creamos el objeto usuario
        $usuario=new Usuario($this->adapter);
         
        
        //Cargamos la vista index y le pasamos valores
        $this->view("index",array(
            "loginError" => "",
            "passError" => "",
            "passChange" => "",
            "createUser" => "",
            "userRepet" => "*Usuario ya existe!" 
        ),1);
    }
     
    public function crear(){   
        $userName = $_POST["userName"];
        $nameUser = $_POST["nameUser"];
        $lastName1 = $_POST["lastName1"];
        $lastName2 = $_POST["lastName2"];
        $startDate = $_POST["startDate"];
        $phone = $_POST["phone"];
        $mail = $_POST["mail"];
        $passwUser = $_POST["passwUser"];
        $usuario = new Usuario($this->adapter);
        $flag = $usuario->validateUserCreate($userName);
        if ($resultado = mysqli_fetch_array($flag)){
            $this->redirect("Usuario", "userRepetido");   
        }else{
            $usuario->setUser($userName);
            $usuario->setNombre($nameUser);
            $usuario->setApellido1($lastName1);
            $usuario->setApellido2($lastName2);
            $usuario->setDate($startDate);
            $usuario->setPhone($phone);
            $usuario->setEmail($mail);
            $usuario->setPassword($passwUser);
            $save=$usuario->save();
            $this->redirect("Usuario", "viewAcceptUser");
        }
    }

    //// validar usuario

    public function loginInvoke(){
        $mail = $_POST["mailLogin"];
        $passwUser = $_POST["passLogin"];
        $usuario = new Usuario($this->adapter);
        $flag = $usuario->validateLogin($mail,$passwUser);
       
        if ($resultado = mysqli_fetch_array($flag)){
            $user =  new Usuario($this->adapter);
            $userLogged = $user->getById($mail);
            $name = $userLogged->nameUSer;
            $this->redirectHome($name);
        }else{
            $this->redirect("Usuario", "viewErrors");
        }
    }

    public  function changePass(){
        $nameUser = $_POST["userCP"];
        $passwUser = $_POST["pswActual"];
        $passwUserNew = $_POST["pswNew1"];
        $passwUserNewConfirm = $_POST["pswNew2"];
        $usuario = new Usuario($this->adapter);
        $flag = $usuario->validateUser($nameUser,$passwUser);
        if ($resultado = mysqli_fetch_array($flag)){
            if ($passwUserNew === $passwUserNewConfirm){
                $usuario->changePassword($nameUser,$passwUser,$passwUserNew);
                $this->redirect("Usuario", "viewAccept");
            }else{
                $this->redirect("Usuario","viewPassError");
            }
        }else{
            $this->redirect("Usuario", "viewErrors");
        }

    }
    public function borrar(){
        if(isset($_GET["userName"])){
            $id=(String)$_GET["userName"];
             
            $usuario=new Usuario($this->adapter);
            $usuario->deleteById($id);
        }
    }
     
     
    public function hola(){
        $usuarios=new UsuariosModel($this->adapter);
        $usu=$usuarios->getUnUsuario();
        var_dump($usu);
    }

}
?>
