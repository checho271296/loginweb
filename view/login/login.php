<?php
  define('CSSPATH', 'view/login/'); //define css path
  $cssItem = 'login.css'; //css item to display
  session_start();
  
?>   
<!doctype html>
<html lang="es">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
         <!-- Bootstrap CSS -->
    <link href="<?php echo (CSSPATH . "$cssItem"); ?>" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    
    <!-- Bootstrap CSS -->
    
    <title>Inicio</title>
  </head>
  <body class="background ">       
        <div class="container marginTop" >
            <div class="containerCard center px-5 pt-5"> 
                <div class="row justify-content-cemailLoginnter colorText ">
                    <div class="col-xl-8 ">
                        <h2> Iniciar sesión</h2>
                        <form name="myform1"  action="<?php echo $helper->url("Usuario","loginInvoke"); ?>" method="post" >
                            <div class="form-row">
                                <div class="col-xl-12 mb-3">
                                    <label for="validationMailS">Correo electrónico</label>
                                    <input type="text" class="form-control " id="validationMailS" name="mailLogin" placeholder="Correo electrónico" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="Correo electrónico no cumple el formato deseado, no debe contener espacios al final." required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-xl-12 mb-3">
                                    <label for="validationPasswordS">Contraseña</label>
                                    <input type="password" class="form-control" id="validationPasswordS" name="passLogin" placeholder="Contraseña" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Debe contener al menos un número y una letra mayúscula y minúscula, y al menos 8 o más caracteres" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-6">
                                    <p id="status" class="colorTextAlert" > 
                                    <?php echo $loginError 
                                    ?>
                                    </p>
                                </div>
                                <div class="col-xl-6">
                                    <p id="status" class="colorTextAlert" > 
                                    <?php echo $passError 
                                    ?>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-6">
                                    <p id="status" class="colorTextOk" > 
                                    <?php echo $passChange 
                                    ?>
                                    </p>
                                </div>
                                <div class="col-xl-3">
                                    <p id="status" class="colorTextOk" > 
                                    <?php echo $createUser 
                                    ?>
                                    </p>
                                </div>
                                <div class="col-xl-3">
                                    <p id="status" class="colorTextAlert" > 
                                    <?php echo $userRepet 
                                    ?>
                                    </p>
                                </div>
                                
                            </div>
                            <div class="row ">
                                <div class="col-xl-12 d-flex justify-content-center">
                                    <button type="submit" class="btn btn-primary  mt-5 mb-5" >Ingresar</button>
                                </div>
                            </div>
                        </form>
                        <div class="row">
                            <div class="col-xl-6">
                                <a href="#" data-toggle="modal" data-target=".bd-forgotPassword-modal-lg" class="colorText">¿Olvidé mi contraseña?</a>
                                <div class="modal fade bd-forgotPassword-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg  ">
                                        <div class="modal-content background pt-5 pb-5">
                                            <form >
                                                <div class="form-row pl-5 pr-5">
                                                    <label for="mailSend"><h2>Ingresar Correo electrónico:</h2></label>
                                                    <input type="email" class="form-control" id="mailSend" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xl-12 pl-5 pr-5">
                                                        <p class="colorText">Se envíara un correo electrónico con una contraseña temporal que se convierte en la actual.</p>
                                                    </div>       
                                                </div>
                                                <div class="row">
                                                    <div class="col-xl-6"><button class="btn btn-primary btn-sm center mt-5" type="submit">Enviar</button></div>
                                                    <div class="col-xl-6"><button class="btn btn-secondary btn-sm center mt-5" type="submit" onClick="Javascript:window.location.href = 'http://localhost/LOGIN/index.php';">Cancelar</button></div>
                                                </div> 
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <a href="#" data-toggle="modal" data-target=".bd-changePassword-modal-lg" class="colorText">Cambiar Contraseña</a>
                                <div class="modal fade bd-changePassword-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg  ">
                                        <div class="modal-content background pt-5 pb-5 text-center">
                                            <form action="<?php echo $helper->url("Usuario","changePass"); ?>" method = "post" >
                                                <div class="form-row pl-5 pr-5 d-flex justify-content-center">
                                                    <label for="userChangePW">Usuario</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="userChangePW">@</span>
                                                        </div>
                                                        <input name ="userCP" type="text" class="form-control " id="userChangePW" placeholder="Usuario" aria-describedby="userChangePW" required>
                                                    </div>
                                                </div>
                                                <div class="form-row pl-5 pr-5 d-flex justify-content-center">
                                                    <label for="actualPassword">Contraseña Actual</label>
                                                    <input  type="password" class="form-control" id="actualPassword" name="pswActual" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Debe contener al menos un número y una letra mayúscula y minúscula, y al menos 8 o más caracteres" required >
                                                </div>
                                                <div class="form-row pl-5 pr-5 d-flex justify-content-center">
                                                    <label for="newPassword">Nueva Contraseña</label>
                                                    <input  type="password" class="form-control" id="newPassword" name="pswNew1" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Debe contener al menos un número y una letra mayúscula y minúscula, y al menos 8 o más caracteres" required >
                                                </div>
                                                <div class="form-row pl-5 pr-5 d-flex justify-content-center">
                                                    <label for="confirmPassword">Confirmar Contraseña</label>
                                                    <input type="password" class="form-control" id="confirmPassword" name="pswNew2" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Debe contener al menos un número y una letra mayúscula y minúscula, y al menos 8 o más caracteres" required >
                                                </div>
                                                <div class="row">
                                                    <div class="col-xl-6"><button class="btn btn-primary btn-sm center mt-5" type="submit">Cambiar</button></div>
                                                    <div class="col-xl-6"><button class="btn btn-secondary btn-sm center mt-5" type="submit" onClick="Javascript:window.location.href = 'http://localhost/LOGIN/index.php';">Cancelar</button></div>
                                                </div> 
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>           
                    </div>
                    <div class="col-xl-4 mx-auto align-self-center pt-4  pl-auto ">
                        <h4 class="text-center"> ¿Aún no estás registrado?</h4>
                        <button type="button" class="btn btn-primary mt-3 d-flex justify-content-center " data-toggle="modal" data-target=".bd-createUser-modal-lg">Registrarse</button>
                        <div class="modal fade bd-createUser-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg  ">
                                <div class="modal-content background pt-5 pb-5">
                                    <div class="containerCard center">
                                        <form name="createUserForm" action="<?php echo $helper->url("Usuario","crear"); ?>" method="post" >
                                            <div class="form-row">
                                                <div class="col-xl-6 mb-3">
                                                <label for="validationServer013">Nombre</label>
                                                <input type="text" class="form-control " id="validationServer013" placeholder="Nombre" name="nameUser"
                                                    required>
                                                <div class="valid-feedback">
                                                    Correcto!
                                                </div>
                                                </div>
                                                <div class="col-xl-6 mb-3">
                                                    <label for="validationServer023">Primer Apellido</label>
                                                    <input type="text" class="form-control" id="validationServer023" placeholder="Primer Apellido" name="lastName1" 
                                                        required>
                                                    <div class="valid-feedback">
                                                        Correcto!
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-xl-6 mb-3">
                                                    <label for="validationServer024">Segundo Apellido</label>
                                                    <input type="text" class="form-control " id="validationServer024" placeholder="Segundo Apellido" name="lastName2"
                                                        required>
                                                </div>
                                                <div class="col-xl-6 mb-3">
                                                    <label for="validationServer034">Número teléfonico</label>
                                                    <input type="tel" class="form-control " id="validationServer034" placeholder="8888-8888" name="phone"
                                                        required>
                                                    <div class="invalid-feedback">
                                                        Por favor indique un Número correcto.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-xl-6 mb-3">
                                                    <label for="validationServerUsername35">Usuario</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="inputGroupPrepend35">@</span>
                                                        </div>
                                                        <input type="text" class="form-control " id="validationServerUsername36" placeholder="Usuario" name="userName"
                                                        aria-describedby="inputGroupPrepend36" required>
                                                        <div class="invalid-feedback">
                                                            Por favor escoger un usuario.
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6 mb-3">
                                                    <label for="validationServer037">Fecha Nacimiento</label>
                                                    <input type="date" class="form-control " id="validationServer037" placeholder="8888-8888" name="startDate"
                                                        required>
                                                    <div class="invalid-feedback">
                                                        Por favor indique una fecha correcta.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-xl-6">
                                                    <label for="exampleInputEmail1">Correo electrónico</label>
                                                    <input name="mail"  type="email" class="form-control" id="exampleInputEmail1" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required>
                                                </div>
                                                <div class="col-xl-6">
                                                    <label for="exampleInputPassword2">Contraseña</label>
                                                    <input name="passwUser" type="password" class="form-control" id="exampleInputPassword2" name="psw" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Debe contener al menos un número y una letra mayúscula y minúscula, y al menos 8 o más caracteres" required>
                                                </div>
                                            </div> 
                                            <div class="row">
                                                <div class="col-xl-6"><button class="btn btn-primary btn-sm center mt-5" type="submit" onclick="validateForm()">Registrarse</button></div>
                                                <div class="col-xl-6"><button class="btn btn-secondary btn-sm center mt-5" type="submit" onClick="Javascript:window.location.href = 'http://localhost/LOGIN/index.php';">Cancelar</button></div>
                                            </div>           
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>   
           </div> 
        </div>
          <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>       
  </body>
</html>