<?php
class Usuario extends EntidadBase{
    private $userName;
    private $nameUser;
    private $lastName1;
    private $lastName2;
    private $startDate;
    private $phone;
    private $mail;
    private $passwUser;
     
    public function __construct($adapter) {
        $table="Usuario";
        parent::__construct($table,$adapter);
    }
     
    public function getUser() {
        return $this->userName;
    }
 
    public function setUser($userName) {
        $this->userName = $userName;
    }
     
    public function getNombre() {
        return $this->nameUser;
    }
 
    public function setNombre($nameUser) {
        $this->nameUser = $nameUser;
    }
 
    public function getApellido1() {
        return $this->lastName1;
    }
 
    public function setApellido1($lastName1) {
        $this->lastName1 = $lastName1;
    }

    public function getApellido2() {
        return $this->lastName2;
    }
 
    public function setApellido2($lastName2) {
        $this->lastName2 = $lastName2;
    }
    public function setDate($startDate) {
        $this->startDate = $startDate;
    }

    public function getDate($startDate) {
        return $this->startDate = $startDate;
    }
    
    public function setPhone($phone) {
        $this->phone = $phone;
    }

    public function getPhone($lastName2) {
        return $this->phone = $phone;
    }
    
    public function getEmail() {
        return $this->email;
    }
 
    public function setEmail($email) {
        $this->mail = $email;
    }
 
    public function getPassword() {
        return $this->passwUser;
    }
 
    public function setPassword($passwUser) {
        $this->passwUser = $passwUser;
    }
 
    public function save(){
        $query="INSERT INTO Usuario (userName,nameUSer,lastName1,lastName2,startDate,phone,mail,passwUser)
                
                VALUES('".$this->userName."',
                        '".$this->nameUser."',
                        '".$this->lastName1."',
                        '".$this->lastName2."',
                        '".$this->startDate."',
                        '".$this->phone."',
                        '".$this->mail."',
                        '".$this->passwUser."')";
        $save=$this->db()->query($query);
        //$this->db()->error;
        return $save;
    }
 
}
?>
